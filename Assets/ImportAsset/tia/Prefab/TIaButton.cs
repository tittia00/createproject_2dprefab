﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TIaButton : MonoBehaviour {

    public SoundManager.SOUND_ITEM soundType = SoundManager.SOUND_ITEM.CLICK;

    Button button;
	// Use this for initialization
	void Start () {
        button = gameObject.GetComponent<Button>();


        GameObject soundManagerObject = GameObject.Find("SoundManager");
        SoundManager soundManager = null;
        if (soundManagerObject != null)
        {
            soundManager = soundManagerObject.GetComponent<SoundManager>();
        }
        button.onClick.AddListener(() => soundManager.PlayOnce((int)soundType));
    }
	
}
