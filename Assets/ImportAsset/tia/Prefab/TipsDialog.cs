﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipsDialog : MonoBehaviour
{

    private Text[] infoText = null;
    private float startTime;
    public float MAX_TIME = 3;
    private float nowTime;

    private int currentTextIndex = 0;

    private void Start()
    {
        infoText = gameObject.GetComponentsInChildren<Text>();

        for (int i=0; i< infoText.Length ;i++)
        {
            infoText[i].gameObject.SetActive(false);
        }
        infoText[currentTextIndex].gameObject.SetActive(true);

        startTime = Time.time;
    }

    private void Update()
    {
        nowTime = (Time.time - startTime);
        if (MAX_TIME <= nowTime)
        {
            nextText();
        }
    }
    private void nextText()
    {
        infoText[currentTextIndex].gameObject.SetActive(false);
        currentTextIndex++;
        if (currentTextIndex >= infoText.Length)
        {
            currentTextIndex = 0;
        }

        infoText[currentTextIndex].gameObject.SetActive(true);
        startTime = Time.time;
    }
}
