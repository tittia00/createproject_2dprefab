﻿using System.IO;
using UnityEditor;
using UnityEngine;


namespace TIAMenuCreatePrefab
{
    internal class TIAMenuCreatePrefab
    {
        [MenuItem("TIAMenu/Create Prefab/Image Frame")]
        private static void CreatePrefabImageFrame()
        {
            CreatePrefab("Assets/ImportAsset/tia/Prefab/Frame.prefab");
        }
        [MenuItem("TIAMenu/Create Prefab/Text Frame Button")]
        private static void CreatePrefabTextFrameButton()
        {
            CreatePrefab("Assets/ImportAsset/tia/Prefab/TextFrameButton.prefab");
        }
        [MenuItem("TIAMenu/Create Prefab/Text Button")]
        private static void CreatePrefabTextButton()
        {
            CreatePrefab("Assets/ImportAsset/tia/Prefab/TextButton.prefab");
        }
        [MenuItem("TIAMenu/Create Prefab/Icon Button")]
        private static void CreatePrefabIconButton()
        {
            CreatePrefab("Assets/ImportAsset/tia/Prefab/Button.prefab");
        }

        [MenuItem("TIAMenu/Create Prefab/Text")]
        private static void CreatePrefabText()
        {
            CreatePrefab("Assets/ImportAsset/tia/Prefab/Text.prefab");
        }


        public static GameObject CreatePrefab(string prefabPath)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject)) as GameObject;
            PrefabUtility.InstantiatePrefab(prefab);
            return prefab;
        }
    }
}