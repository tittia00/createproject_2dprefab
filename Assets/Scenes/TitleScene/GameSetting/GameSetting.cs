﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSetting : MonoBehaviour {

    public GameObject SettingMenu;

    public Slider VolumeSlider;
    public static float VOLUME;
    SoundManager soundManager;
    
    

    // Use this for initialization
    void Start()
    {
        GameObject soundManagerObject = GameObject.Find("SoundManager");
        if (soundManagerObject != null)
        {
            soundManager = soundManagerObject.GetComponent<SoundManager>();
        }

        VolumeSlider.onValueChanged.AddListener(delegate { onVolumeChanged(); });

        GameSetting.VOLUME = PlayerPrefs.GetFloat("GameSetting.VOLUME",0.7f);
        VolumeSlider.value = GameSetting.VOLUME;

        CloseSettingMenu();
    }

    public void OpenSettingMenu () {
        SettingMenu.SetActive(true);
    }

    public void CloseSettingMenu()
    {
        SettingMenu.SetActive(false);
    }
	
    public void onVolumeChanged()
    {
        GameSetting.VOLUME = VolumeSlider.value;
        soundManager.ResetVolume(GameSetting.VOLUME);
        PlayerPrefs.SetFloat("GameSetting.VOLUME", GameSetting.VOLUME);
    }
}
